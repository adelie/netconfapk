"""
NETCONF for APK Distributions server:
    Service management definitions and common routines.

Copyright © 2020 Adélie Software in the Public Benefit, Inc.

Released under the terms of the NCSA license.  See the LICENSE file included
with this source distribution for more information.

SPDX-License-Identifier: NCSA
"""


from abc import ABC, abstractmethod
from enum import Enum


class ServiceStatus(Enum):
    """Represents the current status of a service."""
    Stopped = 0
    """The service is not presently running."""

    Starting = 1
    """The service is in the process of starting."""

    Running = 2
    """The service is presently running."""

    Stopping = 3
    """The service is in the process of stopping."""

    Crashed = 4
    """The service was running, but stopped unexpectedly."""


class Service(ABC):
    """Represents a service on the device.

    This is an abstract base class.  You cannot use it directly; you must use
    the service management module loaded to interact with services on the
    device.
    """

    def __init__(self):
        self._name = None
        self._description = None
        self._enabled = None
        self._status = None
        self._start_time = 0

    @property
    def name(self) -> str:
        """Returns the name of the service."""
        return self._name

    @property
    def description(self) -> str:
        """Returns the description of the service."""
        return self._description

    @property
    def enabled(self) -> bool:
        """Returns whether the service is enabled."""
        return self._enabled

    @property
    def start_time(self) -> int:
        """Returns the time the service was started.

        Note: If the service is not running, this method returns 0.
        """
        return self._start_time

    @abstractmethod
    def status(self) -> ServiceStatus:
        """Returns the current status of the service."""
        return self._status

    @abstractmethod
    def start(self):
        """Start the service."""

    @abstractmethod
    def stop(self):
        """Stop the service."""

    @abstractmethod
    def reload(self):
        """Tell the service to reload its configuration, if possible.

        Not all services support this operation.  If the service does not
        support this operation, it has no effect.
        """

    @abstractmethod
    def restart(self, full: bool):
        """Restart the service.

        :param bool full:
        Determine if this is a 'full' restart or not.  The meaning of a
        full restart is defined by the service manager in use on the
        device.
        """
