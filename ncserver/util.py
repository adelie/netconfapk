"""
NETCONF for APK Distributions server:
    Higher-level utility functions.

Copyright © 2020 Adélie Software in the Public Benefit, Inc.

Released under the terms of the NCSA license.  See the LICENSE file included
with this source distribution for more information.

SPDX-License-Identifier: NCSA
"""


from math import floor
import time

from lxml.etree import QName  # pylint: disable=E0611
from netconf import error

from ncserver.base.modman import MODMAN
from ncserver.base.util import yang_dt_for_timestamp


def maybe_raise_on_invalid_node(xmlns, rpc, node):
    """Determine if this node should cause the module in namespace to raise.

    :param str xmlns:
        The xmlns of the current module.

    :param rpc:
        The RPC causing this lookup to happen.

    :param node:
        The node in question.
    """
    nodens = QName(node).namespace
    if nodens == xmlns:
        raise error.UnknownElementAppError(rpc, node)
    # pylint: disable=W0212
    if nodens not in (mod.M_NS for mod in MODMAN._augments_for_ns(xmlns)):
        raise error.UnknownNamespaceAppError(rpc, node, None)


def system_boot_time() -> str:
    """Return the system's boot time."""
    with open('/proc/uptime', 'r') as upfile:
        raw = upfile.read().split(' ')[0]
        boot = floor(time.time() - float(raw))
        return yang_dt_for_timestamp(boot)


def get_nmsa():
    """Retrieve our NMSA module handle."""
    nmsa_ns = "http://netconf.adelielinux.org/ns/netmgmt"
    nmsa = MODMAN._module_for_ns(nmsa_ns)  # pylint: disable=W0212

    return nmsa
