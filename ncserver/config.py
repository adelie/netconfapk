"""
NETCONF for APK Distributions server:
    Configuration manager module

Copyright © 2020 Adélie Software in the Public Benefit, Inc.

Released under the terms of the NCSA license.  See the LICENSE file included
with this source distribution for more information.

SPDX-License-Identifier: NCSA
"""

import logging
import yaml

from taillight import Signal

from ncserver.base.util import _


CONFIG_RELOADED = Signal(('ConfigManager', 'reloaded'))
"""Signal fired when configuration is reloaded."""


class ConfigManager:
    """The configuration manager for NETCONF for APK Distributions."""

    def __init__(self):
        """Initialise the configuration manager."""
        self._config = {'server': {'port': 830, 'debug': False, 'modules': [],
                                   'users': ['netconf']}}
        self._logger = logging.getLogger("ConfigManager")
        self.reload_config()

    def reload_config(self):
        """Reload the configuration from disk."""
        try:
            with open('/etc/netconf/netconf.conf', 'r') as conf:
                self._config = yaml.safe_load(conf)
        except IOError as exc:
            self._logger.error(_("Couldn't open configuration file: %s"), exc)
        except BaseException as exc:  # pylint: disable=W0703
            self._logger.error(_("Couldn't read configuration file: %s"), exc)
        else:
            CONFIG_RELOADED.call(self)

    def get(self, clade: str, key: str):
        """Retrieve the value for the specified configuration key.

        :param str clade:
        The configuration clade (typically 'server').

        :param str key:
        The configuration key desired.

        :returns str:
        The value of the specified configuration key.

        :raises KeyError:
        The key is not configured or set.
        """
        return self._config[clade][key]

    def get_list(self, clade: str, key: str) -> list:
        """Retrieve a list value for the specified configuration key.

        :param str clade:
        The configuration clade (typically 'server').

        :param str key:
        The configuration key desired.

        :returns list:
        The value of the specified configuration key.

        :raises KeyError:
        The key is not configured or set.

        :raises TypeError:
        The value for this key is not a list.
        """
        value = self._config[clade][key]
        if not isinstance(value, list):
            raise TypeError(_("{}/{} is not a list value").format(clade, key))
        return value
