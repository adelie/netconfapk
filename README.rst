==========================================
 README for NETCONF for APK Distributions
==========================================
:Authors:
 * **A. Wilcox**, principal author
 * **Ariadne Conill**, project manager
:Status:
 Development
:Copyright:
 © 2020 Adélie Software in the Public Benefit, Inc.  NCSA open source license.


.. image:: https://img.shields.io/badge/chat-on%20IRC-blue.svg
   :target: ircs://irc.interlinked.me:6697/#Adelie-Support
   :alt: Chat with us on IRC: irc.interlinked.me #Adelie-Support

.. image:: https://img.shields.io/badge/license-NCSA-lightgrey.svg
   :target: LICENSE
   :alt: Licensed under NCSA license



Introduction
============

This repository contains the source code and documentation for NETCONF for APK
Distributions, a NETCONF server designed for use on Linux distributions that
use the APK package manager.


License
```````
See the LICENSE file in the root directory of this repository for license
information.


Changes
```````
Any changes to this repository must be reviewed before being pushed to the
current branch.



If You Need Help
================

This repository is primarily for system developers.  If you're looking for
help installing or using this project, see our Help Centre on the Web (not yet
available).

.. _`our Help Centre on the Web`: https://help.adelielinux.org/



Usage Requirements
==================

* Python

* The Python ``netconf`` module.



Repository Layout
=================

``ncserver``: Server code
`````````````````````````
The ``ncserver`` directory contains the server code.

``yang-modules``: YANG modules
``````````````````````````````
The ``yang-modules`` directory contains the YANG modules that the server
implements.



Contributing
============

See the CONTRIBUTING.rst_ file in the same directory as this README for
more details on how to contribute to NETCONF for APK Distributions.

.. _CONTRIBUTING.rst: ./CONTRIBUTING.rst



Reporting Issues
================

If you have an issue using NETCONF for APK Distributions, you may view our
BTS_.  You may also `submit an issue`_ directly.

.. _BTS: https://bts.adelielinux.org/buglist.cgi
.. _`submit an issue`: https://bts.adelielinux.org/enter_bug.cgi
