from lxml import etree as et
from ncserver.base.modman import ModuleManager
from netconf.util import elm
mgr = ModuleManager()
node = elm('nc:data')
mgr.collect_operational(node)
print(et.tostring(node, pretty_print=True).decode())
