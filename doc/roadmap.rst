==========================================
 Roadmap and Architecture for NETCONF APK
==========================================


1.0 (2020 Q3)
=============

[X] Module management system
----------------------------

* [X] Track name/revision/namespace, and register with NSMAP

  * Namespace includes prefix, since this is standardised.  i.e.
    ns "urn:ietf:params:xml:ns:yang:ietf-interfaces" -> prefix "if:".

* [X] Track features implemented

* [X] Handle import-only modules

* [ ] Submodule support?  (possibly post-1.0)

* [X] Respond to state requests

* [X] Handle per-module RPC requests


The module management system will be the heart of ``ncserver``.  Similar to
modules in Charybdis/Atheme or GlorIRCd.  Each module will provide a piece of
functionality (in this case, implementation of a YANG module).  Each module
will have a callback for retrieving its relevant config and state information.

The module management system will be responsible for bookkeeping and also
implement the ietf-yang-library module itself.  This exposes the internal
bookkeeping to the NETCONF world for introspecting the server.

This also makes the server more customisable.  If a user does not care about
a specific YANG module, it can just be removed from the list to import.


Outstanding TBDs
````````````````
* None.

Resolved TBDs
`````````````
* The ``modules-state`` requires a ``module-set-id`` which represents the
  unique combination of YANG modules present and loaded in a NETCONF server.
  We have freedom to implement this however we like.  One thought is to take
  the strings of every module's namespace and sha512 the result.  Alternatively
  we could just use Python ``uuid`` to generate a new one on each server start,
  but this would also mean that tooling that caches modules would pull in all
  modules on every server restart.

  **Resolution**: We went with sha256 of a comma-separated list of Python
  module names.  This is expected to be the cleanest way to do this; it allows
  the implementation to provide the same ``module-set-id`` when modules stay
  the same, but also changes ``module-set-id`` if a new module replaces an
  existing one for the same namespace (which may require data to be reloaded).



[X] Logging system
------------------

* [X] Output format

* [X] Configuration

* [X] Event logging

  * [X] auth

    * [X] Connection established

    * [X] Authentication succeess

    * [X] Authentication failure

    * [X] Connection closed

  * [X] operational

    * [X] RPC executed

    * [X] RPC unknown

    * [X] RPC failure

    * [X] RPC success

  * [X] config

    * [X] Configuration editing

    * [X] Configuration edited

    * [X] Configuration reading

    * [X] Configuration operation denied

This system is reponsible for all event logging for the NETCONF APK
project.  See the `logging specification`_ for more details.

.. _`logging specification`: ./logging.rst



[ ] NACM
--------

This feature will require in-depth discussion.



[/] Service management module
-----------------------------

* [X] Design API

* [X] Design YANG model

* [X] Implement module

  * [/] OpenRC

  * [ ] s6?

This module allows administration of services on the system.  See the
separate `service management specification`_ for more details.

.. _`service management specification`: ./service.rst



[/] System module
-----------------

* [/] Configuration nodes

  * [X] Retrieve and set admin contact and system location information.

  * [X] Retrieve and set hostname.

  * [ ] Clock:

    * [ ] Retrieve and set timezone name (``/etc/localtime``).

    * [ ] Determine ``timezone-utc-offset`` from current localtime.

  * [X] DNS configuration

  * [ ] NTP configuration

  * [ ] Local user administration:

    * [ ] SSH key administration (``$HOME/.ssh/authorized_keys``).

* [X] State nodes

  * [X] Platform information.

  * [X] Current date and time.

  * [X] Boot date and time.

* [/] RPCs

  * [ ] Set date/time.

  * [X] Restart.

  * [X] Shutdown.


The system module allows basic administration of system configuration.


Outstanding TBDs
````````````````
* When the admin tries to set ``timezone-utc-offset``, do we make a custom
  zoneinfo file or do we try to match?

* Should there be a group for NETCONF-allowed users?

Resolved TBDs
`````````````
* Do we support NTP?  If so, which implementation?  Prefer chrony because it's
  available on Adélie (openntpd isn't available due to ppc/be issues)

  **Resolution**: Yes, NTP support is desireable.  THe implementation is
  ``ntpsec``.  It has been packaged and tested for Adélie/ppc64, so it appears
  portable.

* Where do we store the ``contact`` and ``location`` node information?

  **Resolution**: Flat files on the disk.

* Can musl be configured for DNS timeout seconds and resolution attempts?
  Would Rich be open to adding this?

  **Resolution**: Yes, musl supports these options in ``/etc/resolv.conf``.

* Do we calculate boot date/time naively (current time - uptime) or do we try
  to ascertain the actual boot date/time from something like ctime of ``/run``?

  **Resolution**: Naive calculation works well.  ctime is not always
  available and may be unreliable (systems that boot with time set to 1970).

* Do we support password-based authentication?

  **Resolution**: No, only key-based authentication.

* If we support password-based authentication, do we have a custom database or
  do we use ``/etc/shadow``?

  **Resolution**: Not applicable; we do not support password-based
  authentication.

* How do we support ``name`` of DNS resolvers?  It is mandatory.

  **Resolution**: Well-defined comment above each nameserver line.  Default
  to 'Resolver' if not specified.

* Do we support RADIUS?

  **Resolution**: No.



[/] Interfaces module
---------------------

* [x] Configuration nodes

  * [x] Name, type

  * [x] Description

  * [x] Enabled

* [/] State nodes

  * [x] Operational state

  * [ ] Time operational state entered

  * [x] Interface index

  * [x] MAC address

  * [ ] Related higher/lower layer interfaces

  * [x] Link speed (bits/second)

  * [x] Statistics (``ifctrstat``)


The interfaces module allows configuration and inspection of the physical layer
of system network interfaces.


Outstanding TBDs
````````````````
* How do we retrieve ``last-change``?

Resolved TBDs
`````````````
* We support ``arbitrary-names``, right?  Pretty sure GRE / tuntap / etc all
  let you name things whatever you want.

  **Resolution**: No.  We do not, as some Linux network tooling cannot properly
  handle UTF-8 names.

* Do we support ``pre-provisioning``?  It would make sense, since /e/n/i and
  /e/c/n both let you just specify whatever configuration you want for whatever
  interfaces you want.

  **Resolution**: Yes.

* Where do we store interface ``description``?

  **Resolution**: /e/n/i can do ``netconf-description`` keyword, /e/c/n (if
  we support it) can do ``description_$IFACE``.



[/] IP module
-------------

* [/] Configuration nodes

  * [ ] Enable/disable IPv4 on an interface

  * [x] Enable/disable IPv4 forwarding on an interface

  * [x] Set IPv4 MTU

  * [x] Retrieve/set IPv4 address(es)

  * [-] Handle neighbour / static ARP cache (set)

  * [x] Enable/disable IPv6 on an interface

  * [x] Enable/disable IPv6 forwarding on an interface

  * [x] Set IPv6 MTU

  * [x] Retrieve/set IPv6 address(es)

  * [x] IPv6 DAD: Neighbour-Solicitation messages sent

  * [x] SLAAC configuration

* [ ] State nodes

  * [/] Origin of IPv4 address(es)

  * [/] Origin of IPv6 address(es)

  * [ ] IPv6 address status


The IP module allows configuration and inspection of the network layer of
system network interfaces.


Outstanding TBDs
````````````````
* Do we have a way to determine what addresses are static vs DHCP?  Can i.e.
  dhcpcd tell us what addresses it configured?

Resolved TBDs
`````````````
* Do we support IPv6 privacy extensions?

  **Resolution**: Yes.




2.0 (2021)
==========

[ ] Software module
-------------------

* [ ] Define model

* [ ] Configuration nodes

  * [ ] APK repositories

    * [ ] URL

    * [ ] Name / description

* [ ] State nodes

  * [ ] Software installed

    * [ ] Name, description, installed size

    * [ ] Repository

* [ ] RPCs

  * [ ] Install software

  * [ ] Deinstall software


The Software module allows configuration and inspection of installed packages
on a system.  This module will need to be properly defined.


Outstanding TBDs
````````````````
* Many.

Resolved TBDs
`````````````
* None.



[ ] NAT module?
---------------

See `ietf-nat module`_.

.. _`ietf-nat module`: https://github.com/YangModels/yang/raw/master/standard/ietf/RFC/ietf-nat%402019-01-10.yang



[ ] VRF/VSI module
------------------

See `ietf-network-instance module`_.

.. _`ietf-network-instance module`: https://github.com/YangModels/yang/raw/master/standard/ietf/RFC/ietf-network-instance%402019-01-21.yang



[ ] Routing module (base)
-------------------------

See `ietf-routing module`_.

.. _`ietf-routing module`: https://github.com/YangModels/yang/raw/master/standard/ietf/RFC/ietf-routing%402018-03-13.yang


Outstanding TBDs
````````````````
* How do we determine when a RIB entry was modified?

Resolved TBDs
`````````````
* Per kernel docs, the Linux kernel supports 242 user-defined RIBs in addition
  to default and main.  Do we want to expose that (feature ``multiple-ribs``)?

  **Resolution**: Yes, we expose feature ``multiple-ribs``.



[ ] Other features
------------------

Some features that have been tossed around that warrant further investigation,
but do not have any concrete proposals or architecture/design.

* [ ] 'Audit' system that can ensure malicious/errant users have not
      manipulated configuration in a way that the NETCONF server can't detect.

* [ ] Integration with something like etckeeper for further audit and change
      logging / documentation capabilities.
