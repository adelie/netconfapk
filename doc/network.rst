=================================================
 Network Management Requirements for NETCONF APK
=================================================
:Author:
  A. Wilcox
:Copyright:
  © 2020 Adélie Software in the Public Benefit, Inc.  NCSA license.
:Status:
  Draft




Introduction
============

This document defines the network management module requirements for the
NETCONF for APK Distributions system.


Definition of terms
-------------------

A basic familiarity with common networking terms is assumed.  Terms
specific to this document are defined as follows.

* ``NETCONF APK``: The NETCONF for APK Distributions system, including the
  base server and modules contained in the netconfapk source distribution.

* ``network management system``: The system used on a computer used to
  control network interfaces.  Common examples include NetworkManager,
  ifupdown-ng, and netifrc.


Intended audience
-----------------

This document is primarily intended to be read by the implementors of
NETCONF APK.  It is also intended to be read by implementors of external
NETCONF APK modules that need to interact with the network manager.  It
may additionally be useful for documentation writers to inform network
administrators who are interacting with NETCONF APK about how its network
management module functions.




Purpose
=======

The network management module allows NETCONF APK to:

* Enumerate configured network interfaces on a computer.

  Note that this enumeration is only for network interfaces with a
  defined configuration.  Interfaces that are present on the system, but
  not configured, shall **not** be present in this enumeration.

* Retrieve configuration information for a specific interface.

* Set configuration information for a specific interface.

The network management module provides a well-defined Python API, used by
NETCONF APK to provide other module functionality.  This API will be used
by many of the IETF Standard network and IP configuration YANG models.

This Python API is implemented as a module to facilitate the abstraction
of the network management system from the implementation of the actual
model modules.  This allows for support of multiple network management
systems with a single model module.

**Note**: If a DHCP client module is not loaded, some features (including
determining if an IP address is sourced from DHCP, and controlling DHCP
usage) may not be available.




Data model definition
=====================

A network interface has the following parameters:

* ``name``, the name of the network interface.  This parameter is
  read-only.

* ``description``, a freeform string describing the network interface set
  by a network administrator.

* ``enabled``, a boolean value describing whether the network interface
  is enabled or not.  Toggling this setting should immediately change the
  state of the interface.

* ``ipv4_enabled``, a boolean value describing whether the network
  interface shall participate in IPv4 networks or not.

* ``ipv4_forwarding``, a bolean value describing whether the network
  interface shall forward IPv4 packets between hosts.

* ``ipv4_mtu``, an integer value describing the Maximum Transmission
  Unit (MTU) for IPv4 packets on the network interface.

* ``ipv6_enabled``, a boolean value describing whether the network
  interface shall participate in IPv6 networks or not.

* ``ipv6_forwarding``, a bolean value describing whether the network
  interface shall forward IPv6 packets between hosts.

* ``ipv6_mtu``, an integer value describing the Maximum Transmission
  Unit (MTU) for IPv6 packets on the network interface.

* ``ipv6_dad_xmit``, an integer value describing the number of DAD probes
  to send when configuring this network interface for IPv6 operation.

* ``ipv6_slaac_globaladdr``, a boolean value describing whether SLAAC
  addresses on the network interface will be global.

* ``ipv6_slaac_tempaddr``, a boolean value describing whether SLAAC
  addresses on the network interface will be temporary.

* ``ipv6_slaac_validlft``, an integer value describing the lifetime of
  temporary SLAAC addresses on the network interface.

* ``ipv6_slaac_preflft``, an integer value describing the preferred
  lifetime of temporary SLAAC addresses on the network interface.




Python API
==========

The network management system integration module is loaded as any other
NETCONF APK module.  The integration module shall implement the
``http://netconf.adelielinux.org/ns/netmgmt`` namespace.  Other modules
access the network management system integration Python API by retrieving
a handle to the module implementing that namespace, then call well-
defined methods on it.



``interface_list()``
--------------------

This method takes no arguments.

Returns a list of strings, where each string is a configured network
interface.



``remove_interface(iface)``
---------------------------

Entirely remove a configured interface from the configuration store.



Transactions
------------

You may queue multiple operations to be saved at one time.  This allows
processing of multiple nodes at once in a single <edit-config/>
operation, for example.


``begin_transaction()``
~~~~~~~~~~~~~~~~~~~~~~~

Starts a transaction.  Every operation performed until ``commit`` is
called will be queued for later.


``commit()``
~~~~~~~~~~~~

Saves all queued operations to configuration storage and configures
network interfaces.


``rollback()``
~~~~~~~~~~~~~~

Erases all queued operations and restores the configuration from when
the transaction began.


``transaction()``
~~~~~~~~~~~~~~~~~

A Python context manager designed to be used with the ``with`` keyword.



Parameters
----------

For manipulating parameters in the section `Data model definition`_,
four methods exist.


``get_param(iface, parameter)``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Retrieve the parameter for the specified interface.  If the parameter is
not set for the specified interface, ``None`` will be returned.


``curr_param(iface, parameter)``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Retrieve the current, live value for the parameter for the specified
interface.  If the parameter is not set for the specified interface,
``None`` will be returned.  The value returned from ``curr_param`` may
differ from ``get_param`` if a network administrator has changed the
value externally, or if the system configuration store is malfunctioning.


``set_param(iface, parameter, value)``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set the parameter for the specified interface.  If a transaction is not
active, the parameter is immediately set on the network interface.


``unset_param(iface, parameter)``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Unset the parameter for the specified interface.  If a transaction is not
active, the parameter is immediately unset on the network interface.
This may involve resetting a value to a system-defined default value.



Addresses
---------

The following methods exist for manipulating addresses on a network
interface.


``list_addresses(iface)``
~~~~~~~~~~~~~~~~~~~~~~~~~

Retrieve all configured addresses for the specified network interface.
The return type is always an iterable; if no addresses are configured,
the iterable will be empty.  Each element is a string in the format
"``ip/prefix``" where ``ip`` is the IP address and ``prefix`` is the
subnet prefix.


``add_address(iface, type, addr, prefix)``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Add a statically configured address of the specified ``type`` to the
specified network interface.

``type`` must be ``socket.AF_INET`` for IPv4 or ``socket.AF_INET6`` for
IPv6.  ``addr`` must be the IP address as a string.  ``prefix`` may be
an integer or string containing only numeric digits that represents the
desired subnet prefix for the address.

If a transaction is not active, the address is immediately added to the
network interface.


``remove_address(iface, addr)``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Remove a statically configured address from the specified network
interface.

``addr`` may optionally have a ``prefix`` component, but it will be
ignored and will *not* be checked for accuracy before the address is
removed.

If a transaction is not active, the address is immediately removed from
the network interface.
