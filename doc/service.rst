=================================================
 Service Management Requirements for NETCONF APK
=================================================
:Author:
  A. Wilcox
:Copyright:
  © 2020 Adélie Software in the Public Benefit, Inc.  NCSA license.
:Status:
  Draft




Introduction
============

This document defines the service management module requirements for the
NETCONF for APK Distributions system.


Definition of terms
-------------------

* ``NETCONF APK``: The NETCONF for APK Distributions system, including the
  base server and modules contained in the netconfapk source distribution.

* ``service``: A long-running process (or ``daemon``) on a system.

* ``service manager``: The component of the operating system that controls
  daemons.


Intended audience
-----------------

This document is primarily intended to be read by the implementors of
NETCONF APK.  It is also intended to be read by implementors of external
NETCONF APK modules that need to interact with the service manager.  It
may additionally be useful for documentation writers to inform network
administrators who are interacting with NETCONF APK about how its service
management module functions.




Purpose
=======

The service management module allows NETCONF APK to query service status,
start and stop services, list running services, and list available
services on a system.

The service management module provides two methods of interaction.  The
first is a well-defined Python API, used by NETCONF APK to provide other
module functionality.  For example, this includes /system/ntp/enabled.
The second is a YANG model that allows network administrators to directly
interrogate the system via NETCONF on current service status, and to
request services to start and stop in a service-independent manner using
a well-defined RPC.




Data model definition
=====================

A service has at least the following properties:

* ``name``, the name of the service.

* ``description``, a meaningful description of what the service does or
  provides.  This may be the name of the service if the service
  description does not provide or support a description field.

* ``enabled``, a boolean value describing whether the service is enabled
  or not.  When a service is enabled, it is automatically started when the
  system boots.

* ``status``, an enumeration value of the current status of the service.

  The ``ServiceStatus`` enumeration has at least the following values:

  * ``stopped``: the service is not presently started.

  * ``starting``: the service is in the process of starting up.

  * ``running``: the service is presently running.

  * ``stopping``: the service is in the process of stopping.

  * ``crashed``: the service was started, but stopped unexpectedly.
    Not all service managers support this enumeration value.

  Additional values may be defined depending on the service manager
  currently in use on the system.

* ``start-time``, a timestamp when the service was started.  This property
  is only available if ``status`` is ``started``.

Additional properties may be available depending on the service manager
currently in use on the system.

All property values are provided directly by the service manager; the
NETCONF APK service management module does not attempt any form of
translation or emulation of the values returned by the service manager.
Additionally, ``status`` state transitions are explicitly defined by the
service manager currently in use on the system; no guarantee is made on
the ordering of transitions nor which transitions are valid and invalid.




Python API
==========

The service management module is loaded as any other NETCONF APK module.
It implements the ``http://netconf.adelielinux.org/ns/service`` namespace.
Other modules access the service management Python API by retrieving a
handle to the module implementing that namespace, and then calling well-
defined methods on it.



``service_list()``
------------------

This method takes no arguments.

Returns an iterable of ``Service`` objects as described in the section
`Python Service objects`_.



``info(service)``
-----------------

This method takes a single argument, ``service``.  This argument is a
string and must contain the name of a service present on the system.

This method returns the Service object associated with the service given
as the argument, as described in the section `Python Service objects`_.

If the argument is not a string or does not the contain the name of a
service present on the system, a Python ``NameError`` will be raised.



Python Service objects
----------------------

A Python ``Service`` object has all properties defined in the section
`Data model definition`_.  Any dash (``-``) characters in a property name
are replaced with underscore (``_``) characters.

The ``Service`` object contains the following methods:


``status()``
~~~~~~~~~~~~

This method queries the service manager for the current status of the
service associated with the Service object.

This method returns a value from the ``ServiceStatus`` enumeration defined
under the ``status`` property in the `Data model definition`_ section.


``start()``
~~~~~~~~~~~

This method starts the service associated with the Service object.

This method has no effect if the status of the service is ``starting`` or
``started``.

This method returns immediately with no value.  To determine if a service
started successfully, you must query the service's status.


``stop()``
~~~~~~~~~~

This method stops the service associated with the Service object.

This method has no effect if the status of the service is ``stopping``,
``stopped`` or ``crashed``.

This method returns immediately with no value.  To determine if a service
stopped successfully, you must query the service's status.


``reload()``
~~~~~~~~~~~~

This method reloads the service associated with the Service object.

This method has no effect unless the status of the service is ``started``.

This method returns immediately with no value.


``restart(full)``
~~~~~~~~~~~~~~~~~

This method restarts the service associated with the Service object.

This method has no effect if the status of the service is ``stopping`` or
``starting``.

If the status of the service is not ``started``, the service will be
started without a stop attempt.

The argument ``full`` determines if the restart is a full restart or not.
The meaning of a full restart is implementation-defined.  The argument
defaults to ``True`` if it is not specified.

This method returns immediately with no value.  To determine if a service
restarted successfully, you must query the service's status.




YANG Model
==========

Data model
----------

The data model for system services has the following structure:

::

  +--rw services
     +--rw service* [name]
        +--rw name           string
        +--ro description?   string
        +--rw enabled        boolean
        +--ro status         service-status
        +--ro start-time?    yang:date-and-time

Module
------

See `adelie-services.yang`_.

.. _`adelie-services.yang`: ../yang-modules/adelie-services.yang
