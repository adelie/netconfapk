======================================
 Logging Requirements for NETCONF APK
======================================
:Author:
  A. Wilcox
:Copyright:
  © 2020 Adélie Software in the Public Benefit, Inc.  NCSA license.
:Status:
  Draft




Introduction
============

This document defines the logging requirements for the NETCONF for APK
Distributions system.


Definition of terms
-------------------

* ``NETCONF APK``: The NETCONF for APK Distributions system, including the
  base server and modules contained in the netconfapk source distribution.

* ``stderr``: The standard error output stream as defined by POSIX.


Intended audience
-----------------

This document is primarily intended to be read by the implementors of
NETCONF APK.  It may additionally be useful for documentation writers to
inform network administrators who are interacting with NETCONF APK about
how its logging system functions.




Architecture
============

The logging system processes messages with three fields: Category, Level,
and Message.  The message's Category indicates what subsystem is emitting
the message.  The message's Level indicates the urgency of the message,
and may be one of the following six values: Critical, Error, Warning,
Notice, Info, and Debug.  The message's Message is the text being logged.

The system outputs every message processed to ``stderr``, with the
following format:

::

    <iso-date> : <device> : <category> : <level> : <message>

iso-date is defined as an ISO 8601 extended timestamp, of the format:

::

    YYYY-MM-DDThh:mm:ss

``device`` is the hostname of the device running the logging system.




Configuration
=============

The ``netconf.conf`` YAML configuration file will be read for a scalar
``loglevel`` present in the top-level ``server`` node, which defines the
lowest log level to output.  The default, if the ``loglevel`` scalar is
not set, is ``notice``.

The ``loglevel`` node is case-insensitive.




Categories and Log Messages
===========================

Each following section defines a logging category for the NETCONF APK
system.  Under each logging category, each event that would generate a
message in that category will be described.

This list is not exclusive.  Underlying system libraries may log
additional messages that are not included in this list.


auth
----

Messages in this category concern authentication.  This includes both
successful and failed authentication attempts to the server.

Connection established
``````````````````````
This event is triggered when a connection is established, before
authentication is attempted.

:Format:
  Connection from %{ip}:%{srcport} established with client %{client_ver}
:Level:
  Info

Authentication succeess
```````````````````````
This event is triggered when authentication is successful and a user is
granted access to the system.

:Format:
  Authentication granted to %{user} from %{ip}:%{srcport}, session %{sessid}
:Level:
  Notice

Authentication failure
``````````````````````
This event is triggered when authentication has failed.

:Format:
  Authentication failed for %{user} from %{ip}:%{srcport}
:Level:
  Error

Connection closed
`````````````````
This event is triggered when a client disconnects.

:Format:
  Session %{sessid} for %{user} closed
:Level:
  Info


operational
-----------

Messages in this category concern operations carried out.  This includes
standard operations such as ``edit-config`` and also RPCs such as
``system-restart``.

RPC executed
````````````
This event is triggered when any RPC is sent to the system.

:Format:
  RPC %{rpc} invoked by %{user} on session %{sessid}
:Level:
  Info

RPC unknown
```````````
This event is triggered when an RPC is received that the system does not
recognise as a valid RPC.

:Format:
  RPC %{rpc} invoked by %{user} on session %{sessid} is unknown
:Level:
  Warning

RPC failure
```````````
This event is triggered when an RPC encounters an error during execution.

:Format:
  RPC %{rpc} invoked by %{user} on session %{sessid} encountered error %{error}
:Level:
  Error

RPC success
```````````
This event is triggered when an RPC is executed successfully.

:Format:
  RPC %{rpc} invoked by %{user} on session %{sessid} completed successfully
:Level:
  Info


config
------

Messages in this category concern configuration data.  This includes
reading and writing to any NETCONF datastore.

Configuration editing
`````````````````````
This event is triggered just before ``edit-config`` is executed.

:Format:
  %{user} on session %{sessid} is editing configuration: %{xml}
:Level:
  Debug

Configuration edited
````````````````````
This event is triggered when ``edit-config`` nodes are being processed by
their respective NETCONF modules.

:Format:
  %{user} on session %{sessid} is editing %{node}: %{module-specific information}
:Level:
  Notice

Configuration reading
`````````````````````
This event is triggered just before ``get-config`` or ``get`` is executed.

:Format:
  %{user} on session %{sessid} is requesting configuration: %{xml}
:Level:
  Debug

Configuration operation denied
``````````````````````````````
This event is triggered when a user attempts an operation on a datastore
that is not permitted by the current NACM policy.

:Format:
  %{user} on session %{sessid} attempted %{operation} on %{node}, not permitted by NACM
:Level:
  Error
