======================================================
 Contribution Guide for NETCONF for APK Distributions
======================================================
:Author:
  * **A. Wilcox**, documentation writer
:Copyright:
  © 2020 Adélie Software in the Public Benefit Inc.  NCSA open source licence.



Code Style
==========

All code must comply with PEP 8.



Tests
=====

At this time, a feasible way for testing this project is not known.



Contributing Changes
====================

This section describes the usual flows of contribution to this repository.
For a detailed description of how to contribute to Adélie software, review the
Handbook_.

.. _Handbook: https://help.adelielinux.org/html/devel/


GitLab Pull Requests
````````````````````

#. If you do not already have a GitLab account, you must create one.

#. Create a *fork* of the NETCONF repository.  For more information, consult
   the GitLab online documentation.

#. Clone your forked repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the commands ``git commit -S`` and
   ``git push``.

#. Visit your forked repository in a Web browser.

#. Choose the *Create Pull Request* button.

#. Review your changes to ensure they are correct, and then submit the form.
